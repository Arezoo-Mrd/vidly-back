// const http = require('http')
// const server = http.createServer((req, res) => {
//     if (req.url === '/') {
//         res.write('Hello world')
//         res.end()
//     }


//     if (req.url === '/api/courses') {
//         res.write(JSON.stringify([1, 2, 3]));
//         res.end()
//     }
// })

// server.listen(8800)
// console.log()
const debug = require('debug')('app:startup')
const config = require('config')
const morgan = require('morgan')
const helmet = require("helmet");
const courses = require('./routes/courses');
const home = require('./routes/home')
const express = require('express');
const logger = require('./middleware/logger');
const auth = require('./auth');
const app = express()

app.set('view engine', 'pug')
app.set('views', './views')

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static('public'))
app.use(helmet())
app.use('/', home)
app.use('/api/courses', courses)


// Configuration
console.log('Application Name: ', config.get('name'))
console.log('Mail Server: ', config.get('mail.host'))
console.log('Mail Server Password: ', config.get('mail.password'))

if (app.get('env') === 'development') {
    app.use(morgan('tiny'))
    debug('Morgan enabled...')
}



app.use(logger);

app.use(auth);



// PORT
const port = process.env.PORT || 8800
app.listen(port, () => {
    console.log(`Listening on port ${port}... `)
})


