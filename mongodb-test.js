const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/playground')
    .then(() => console.log('connected to mongoDB...'))
    .catch((err) => console.log('could not to connect to mongoDB...', err))


const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [String],
    date: { type: Date, default: Date.now },
    isPublish: Boolean
})

// classes and object
// Course ,  nodeCourse

const Course = mongoose.model('Course', courseSchema);

async function createCourse() {
    const course = new Course({
        name: 'React.js Course',
        author: 'A-Mrd',
        tags: ['css', 'frontend'],
        isPublish: true
    })

    const result = await course.save();
    console.log('result: ', result)
}

async function getCourses() {

    // ***** comparing
    // eq (equal)
    // ne (not equal)
    // gt (greater than)
    // gte (greater than or equal to)
    // lt (less than)
    // lte (less than or equal to)
    // in
    // nin (not in)


    // ******Logical
    // or
    // and

    const pageNumber = 2;
    const pageSize = 10;
    //  /api/courses?pageNumber2&pageSize=10


    const courses = await Course
        .find({ author: 'Arezoo Moradi', isPublish: true })
        // .find({ price: { $gte: 10 , $lte: 20 } })
        // .find({ price: { $in: [10, 15, 20] } })
        // .find().or([{ author: 'Arezoo Moradi' }, { isPublish: true }])
        // .and([{ author: 'Arezoo Moradi'} , {isPublish: true}])

        //****  regex
        // Start with Arezoo
        // .find({ author: /^Arezoo/ })
        // ends with Mrd
        // .find({ author: /MORADI$/i })

        // Contains Arezoo
        // .find({ author: /.*Arezoo.*/i })
        .skip((pageNumber - 1) * pageSize)
        .limit(pageSize)
        .sort({ name: -1 })
        .select({ name: 1, tags: 1 })
    console.log("🚀 ~ file: mongodb-test.js ~ line 35 ~ getCourses ~ courses", courses)
}

getCourses()


